"use strict";

const grid = document.getElementById('grid');
const boxes = document.getElementsByClassName('box');
const score = document.getElementById('score');

const circle = createCircle();

function createCircle() {
    const div = document.createElement('div');
    div.className = 'circle';
    return div;
}

// excludes box with circle
function getRandomBox() {
    return boxes[Math.floor(Math.random() * boxes.length)];
}

function getRandomAvailableBox() {
    if (boxes.length === 1) {
        return boxes[0];
    }

    const box = getRandomBox();
    return box !== getActiveBox() ? box : getRandomAvailableBox();
}

function switchCircle(box, circle) {
    box.append(circle);
}

function incScore() {
    score.innerText++;
}

// function which adds a new box
function addBox() {
    const div = document.createElement('div');
    div.className = 'box';
    grid.appendChild(div);
}

function addBoxes(number) {
    for (let i = 0; i < number; i++) {
        addBox();
    }
}

// function which gets a box where circle is located
function getActiveBox() {
    for (let box of boxes) {
        if (box.children.length > 0) {
            return box;
        }
    }
}

// function which gets a box where circle is located
function getActiveBox2() {
    return circle.parentElement;
}

// functions which sets number of grid columns
function setGridColumns(number) {
    grid.style.gridTemplateColumns = '100px '.repeat(number).trim();
}

// function which sets number of grid rows
function setGridRows(number) {
    grid.style.gridTemplateRows = '100px '.repeat(number).trim();
}

// function which changes any element color
function setElementColor(el, color) {
    el.style.backgroundColor = color;
}

function setElementsColor(arr, color) {
    for (let el of arr) {
        setElementColor(el, color);
    }
}

function getRandomColor() {
    return 'rgb(' + [randomize(), randomize(), randomize()].join(',') + ')';
}

function randomize() {
    return Math.floor(Math.random() * 256);
}

// function which reduces element size
function shrinkElement(el, percent) {
    el.style.width = Number(window.getComputedStyle(el).width.replace('px', '')) * (1 - percent / 100) + 'px';
    el.style.height = Number(window.getComputedStyle(el).height.replace('px', '')) * (1 - percent / 100) + 'px';
}

// function which increases element size
function growElement(el, percent) {
    el.style.width = (el.style.width === '' ? 100 : el.style.width.replace('px', '')) * (1 + percent / 100) + 'px';
    el.style.height = (el.style.height === '' ? 100 : el.style.height.replace('px', '')) * (1 + percent / 100) + 'px';
}

function growElement2(el, percent) {
    el.style.width = Number(window.getComputedStyle(el).width.replace('px', '')) * (1 + percent / 100) + 'px';
    el.style.height = Number(window.getComputedStyle(el).height.replace('px', '')) * (1 + percent / 100) + 'px';
}

// function which reduces elements size
function shrinkElements(arr, percent) {
    for (let el of arr) {
        shrinkElement(el, percent);
    }
}

// function which increases elements size
function growElements(arr, percent) {
    for (let el of arr) {
        growElement(el, percent);
    }
}

function reset() {
    grid.innerHTML = '';
    levelScore = 0;
}

circle.onclick = function() {
    switchCircle(getRandomAvailableBox(), this);
    incScore();

    levelScore += 1;

    if (levelScore === currentLevel.goal) {
        nextLevel();
    }
}

setLevel(0);