const level = document.getElementById('level');

const levels = [{
    greeting: 'Welcome to level 0!',
    time: 10,
    goal: 1,
    init: function() {
        addBoxes(4);
        setGridColumns(4);
        shrinkElements(boxes, 20);

        for (let i = 1; i < boxes.length; i += 2) {
            shrinkElement(boxes[i], 60);
        }
    }
}, {
    greeting: 'Welcome to level 1!',
    time: 5,
    goal: 1,
    init: function() {
        addBoxes(15);
        setGridColumns(2);
        shrinkElements(boxes, 35);

        for (let i = 1; i < boxes.length; i += 3) {
            shrinkElement(boxes[i], 30);
        }
    }
}];

let currentLevel = levels[0];
let levelScore = 0;

function setLevel(number) {
    reset();
    currentLevel = levels[number];
    currentLevel.init();
    level.innerHTML = number;
    switchCircle(getRandomBox(), circle);
    console.log(currentLevel.greeting);
}

// console.log('you win!');
function nextLevel() {
    if (currentLevel === levels[levels.length - 1]) {
        console.log('you win!!!')
    } else {
        setLevel(levels.indexOf(currentLevel) + 1);
    }
}